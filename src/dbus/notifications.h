#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QTimer>
#include <QList>
#include <Sailfish/Secrets/secretmanager.h>
#include <keepalive/backgroundactivity.h>
#include <nemonotifications-qt5/notification.h>
#include "sailtrixsignals.h"

class Notifications : public QObject
{
    Q_OBJECT
public:
    explicit Notifications(QObject *parent = nullptr, SailtrixSignals* _sig = nullptr);
    ~Notifications();
    void pause();
    void resume();
    bool isStopped();
signals:
private:
    Sailfish::Secrets::SecretManager m_secretManager;
    QString m_hs_url;
    QString m_access_token;
    QNetworkAccessManager* manager;
    BackgroundActivity* activity;
    SailtrixSignals* sig;
    QString next;
    qint64 start_time;
    BackgroundActivity::Frequency freq = BackgroundActivity::ThirtySeconds;
    bool m_disabled = false;
    QList<Notification*> notification_list;
public slots:
    void disable();
    void enable();
    void changeFrequency(int new_freq);
private slots:
    void every30Seconds();
    void processNotifications(QNetworkReply* reply);

};

#endif // NOTIFICATIONS_H
